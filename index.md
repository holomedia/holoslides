---
---
# holoDocument Presentations (Source)


This is where *all* our presentations slides resides ...

* Source [Open documents](https://gitlab.com/holomedia/holoslides/-/tree/source/ODPs) (Impress)
* [Portable Documents](PDFs) (PDF)
