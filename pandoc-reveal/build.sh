#

#url=https://cdn.jsdelivr.net/npm/reveal.js@3.9
#url=https://cdn.rawgit.com/hakimel/reveal.js/master
#url=https://unpkg.com/@pluginjs/reveal
url=https://cdn.jsdelivr.net/gh/hakimel/reveal.js@3.9


if [ "x$url" = 'x' ]; then
   #url=https://revealjs.com
   url=https://cdn.jsdelivr.net/npm/reveal.js@3.9
   if [ ! -d reveal.js ]; then
      git clone -r v3.9 https://github.com/hakimel/reveal.js.git
      url=./reveal.js
   fi
fi
pandoc -t revealjs --template revealjs.tpl -s -o djangoslide.html django-intro.md -V revealjs-url=$url
