# Pandoc + RevealJS

see also [1](https://ibcomputing.com/build-presentations-with-reveal-js-and-markdown/)

```md
% Django Introduction
% Mujeeb Rahman K
% February 09, 2020

# Django

## Why Django
* Django is a Web framework written in Python
* Don't reinvent the wheel.
* 216 K packages in python package index
```

## html conversion

```sh
git clone -b v3.9 https://github.com/hakimel/reveal.js.git
pandoc -t revealjs -s -o djangoslide.html django-intro.md -V revealjs-url=./reveal.js
```

